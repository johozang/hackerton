package com.ji.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

public class ShowAllPanel extends JPanel {
	
	private JLabel iotTrackLbl,multiTrackLbl,systemTrackLbl,aiTrackLbl,virtualTrackLbl;

	private JProgressBar iotTrackPb1,multiTrackPb1,systemTrackPb1,aiTrackPb1,virtualTrackPb1;
	private JProgressBar iotTrackPb2,multiTrackPb2,systemTrackPb2,aiTrackPb2,virtualTrackPb2;
	
	public ShowAllPanel() {
		setPreferredSize(new Dimension(1900,800));
		
		iotTrackLbl = new JLabel("IOT Track");
		iotTrackLbl.setLayout(null);
		iotTrackLbl.setFont(new Font("",Font.BOLD, 15));
		iotTrackLbl.setBounds(10,10,200,50);
		add(iotTrackLbl);
		
		multiTrackLbl= new JLabel("Multi Media Track");
		multiTrackLbl.setFont(new Font("",Font.BOLD, 15));
		multiTrackLbl.setLayout(null);
		multiTrackLbl.setBounds(10,180,200,50);
		add(multiTrackLbl);
		
		systemTrackLbl= new JLabel("System App Track");
		systemTrackLbl.setLayout(null);
		systemTrackLbl.setFont(new Font("",Font.BOLD, 15));
		systemTrackLbl.setBounds(10,350,200,50);
		add(systemTrackLbl);
		
		aiTrackLbl= new JLabel("AI Track");
		aiTrackLbl.setLayout(null);
		aiTrackLbl.setFont(new Font("",Font.BOLD, 15));
		aiTrackLbl.setBounds(10,520,200,50);
		add(aiTrackLbl);
		
		virtualTrackLbl= new JLabel("Virtual Track");
		virtualTrackLbl.setLayout(null);
		virtualTrackLbl.setFont(new Font("",Font.BOLD, 15));
		virtualTrackLbl.setBounds(10,690,200,50);
		add(virtualTrackLbl);
		
		
		iotTrackPb1 = new JProgressBar();
		iotTrackPb1.setLayout(null);
		iotTrackPb1.setBounds(250,10,1000,30);
		iotTrackPb1.setValue(10);
		iotTrackPb1.setForeground(new Color(171,0,19));
		add(iotTrackPb1);
		
		iotTrackPb2 = new JProgressBar();
		iotTrackPb2.setLayout(null);
		iotTrackPb2.setBounds(250,50,1000,30);
		iotTrackPb2.setValue(10);
		iotTrackPb2.setForeground(new Color(171,0,19));
		add(iotTrackPb2);
		
		multiTrackPb1 = new JProgressBar();
		multiTrackPb1.setLayout(null);
		multiTrackPb1.setBounds(250,180,1000,30);
		multiTrackPb1.setValue(20);
		multiTrackPb1.setForeground(new Color(171,0,19));
		add(multiTrackPb1);
		
		multiTrackPb2 = new JProgressBar();
		multiTrackPb2.setLayout(null);
		multiTrackPb2.setBounds(250,220,1000,30);
		multiTrackPb2.setValue(20);
		multiTrackPb2.setForeground(new Color(171,0,19));
		add(multiTrackPb2);
		
		systemTrackPb1 = new JProgressBar();
		systemTrackPb1.setLayout(null);
		systemTrackPb1.setBounds(250,350,1000,30);
		systemTrackPb1.setValue(30);
		systemTrackPb1.setForeground(new Color(171,0,19));
		add(systemTrackPb1);
		
		systemTrackPb2 = new JProgressBar();
		systemTrackPb2.setLayout(null);
		systemTrackPb2.setBounds(250,390,1000,30);
		systemTrackPb2.setValue(30);
		systemTrackPb2.setForeground(new Color(171,0,19));
		add(systemTrackPb2);
		
		aiTrackPb1 = new JProgressBar();
		aiTrackPb1.setLayout(null);
		aiTrackPb1.setBounds(250,520,1000,30);
		aiTrackPb1.setValue(40);
		aiTrackPb1.setForeground(new Color(171,0,19));
		add(aiTrackPb1);
		
		aiTrackPb2 = new JProgressBar();
		aiTrackPb2.setLayout(null);
		aiTrackPb2.setBounds(250,560,1000,30);
		aiTrackPb2.setValue(40);
		aiTrackPb2.setForeground(new Color(171,0,19));
		add(aiTrackPb2);
		
		virtualTrackPb1 = new JProgressBar();
		virtualTrackPb1.setLayout(null);
		virtualTrackPb1.setBounds(250,690,1000,30);
		virtualTrackPb1.setValue(50);
		virtualTrackPb1.setForeground(new Color(171,0,19));
		add(virtualTrackPb1);
		
		virtualTrackPb2 = new JProgressBar();
		virtualTrackPb2.setLayout(null);
		virtualTrackPb2.setBounds(250,730,1000,30);
		virtualTrackPb2.setValue(50);
		virtualTrackPb2.setForeground(new Color(171,0,19));
		add(virtualTrackPb2);
	}
}
