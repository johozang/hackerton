package com.ji.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;


public class SearchStud extends JPanel {
	
	private JTextField studIdField;
	private JPanel show;
	
	private JLabel studIdLbl,studNameShowLbl, studIdShowLbl, studMajorShowLbl, studSemesterShowLbl;
	private JLabel studNameLbl, studIDLbl, studMajorLbl, studSemesterLbl;
	
	private JLabel T1opt,T1ness,T2opt,T2ness,T3opt,T3ness,T4opt,T4ness,T5opt,T5ness;
	
	private JButton track1, track2, track3, track4, track5;
	
	private JProgressBar T1Pbness,T2Pbness,T3Pbness,T4Pbness,T5Pbness;
	private JProgressBar T1Pbopt,T2Pbopt,T3Pbopt,T4Pbopt,T5Pbopt;
	
	private BtnListener btnL;
	
	
	
	private JButton searchBtn;
	
	
	public SearchStud() {
		setPreferredSize(new Dimension(1900,800));
		btnL = new BtnListener();
		
		show = new JPanel();
		show.setLayout(null);
		show.setBounds(200,200,1000,400);
		show.setVisible(false);
		add(show);
		
		studIdLbl = new JLabel("학번 입력 : ");
		studIdLbl.setLayout(null);
		studIdLbl.setFont(new Font("",Font.BOLD, 25));
		studIdLbl.setBounds(600,10,200,30);
		add(studIdLbl);
		
		studIdField = new JTextField();
		studIdField.setLayout(null);
		studIdField.setBounds(820,10,200,30);
		studIdField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		add(studIdField);
		
		searchBtn = new JButton("검색");
		searchBtn.setLayout(null);
		searchBtn.setFont(new Font("",Font.BOLD, 25));
		searchBtn.setBounds(1100, 10, 100, 30);
		searchBtn.addMouseListener(btnL);
		add(searchBtn);
		
		studNameShowLbl = new JLabel("이름");
		studNameShowLbl.setLayout(null);
		studNameShowLbl.setFont(new Font("",Font.BOLD, 25));
		studNameShowLbl.setBounds(480,55,200,30);
		studNameShowLbl.setVisible(false);
		add(studNameShowLbl);
		
		studIdShowLbl = new JLabel("학번");
		studIdShowLbl.setLayout(null);
		studIdShowLbl.setFont(new Font("",Font.BOLD, 25));
		studIdShowLbl.setBounds(690,55,200,30);
		studIdShowLbl.setVisible(false);
		add(studIdShowLbl);
		
		studMajorShowLbl = new JLabel("학과");
		studMajorShowLbl.setLayout(null);
		studMajorShowLbl.setFont(new Font("",Font.BOLD, 25));
		studMajorShowLbl.setBounds(900,55,200,30);
		studMajorShowLbl.setVisible(false);
		add(studMajorShowLbl);
		
		studSemesterShowLbl = new JLabel("학기");
		studSemesterShowLbl.setLayout(null);
		studSemesterShowLbl.setFont(new Font("",Font.BOLD, 25));
		studSemesterShowLbl.setBounds(1100,55,200,30);
		studSemesterShowLbl.setVisible(false);
		add(studSemesterShowLbl);
		
		track1 = new JButton("track1");
		track1.setFont(new Font("",Font.BOLD, 25));
		track1.setLayout(null);
		track1.setBounds(250,110,200,30);
		track1.setVisible(false);
		add(track1);
		
		T1Pbness = new JProgressBar();
		T1Pbness.setLayout(null);
		T1Pbness.setBounds(250,180,450,30);
		T1Pbness.setValue(20);
		T1Pbness.setForeground(new Color(171,0,19));
		T1Pbness.setVisible(false);
		add(T1Pbness);
		
		T1Pbopt = new JProgressBar();
		T1Pbopt.setLayout(null);
		T1Pbopt.setBounds(250,230,450,30);
		T1Pbopt.setValue(20);
		T1Pbopt.setForeground(new Color(171,0,19));
		T1Pbopt.setVisible(false);
		add(T1Pbopt);
	
		track2 = new JButton("track2");
		track2.setFont(new Font("",Font.BOLD, 25));
		track2.setLayout(null);
		track2.setBounds(1100,110,200,30);
		track2.setVisible(false);
		add(track2);
		
		T2Pbness = new JProgressBar();
		T2Pbness.setLayout(null);
		T2Pbness.setBounds(1100,180,450,30);
		T2Pbness.setValue(20);
		T2Pbness.setForeground(new Color(171,0,19));
		T2Pbness.setVisible(false);
		add(T2Pbness);
		
		T2Pbopt = new JProgressBar();
		T2Pbopt.setLayout(null);
		T2Pbopt.setBounds(1100,230,450,30);
		T2Pbopt.setValue(20);
		T2Pbopt.setForeground(new Color(171,0,19));
		T2Pbopt.setVisible(false);
		add(T2Pbopt);
		
		track3 = new JButton("track3");
		track3.setFont(new Font("",Font.BOLD, 25));
		track3.setLayout(null);
		track3.setBounds(50,410,200,30);
		track3.setVisible(false);
		add(track3);
		
		T3Pbness = new JProgressBar();
		T3Pbness.setLayout(null);
		T3Pbness.setBounds(50,480,400,30);
		T3Pbness.setValue(20);
		T3Pbness.setForeground(new Color(171,0,19));
		T3Pbness.setVisible(false);
		add(T3Pbness);
		
		T3Pbopt = new JProgressBar();
		T3Pbopt.setLayout(null);
		T3Pbopt.setBounds(50,530,400,30);
		T3Pbopt.setValue(20);
		T3Pbopt.setForeground(new Color(171,0,19));
		T3Pbopt.setVisible(false);
		add(T3Pbopt);

		track4 = new JButton("track4");
		track4.setFont(new Font("",Font.BOLD, 25));
		track4.setLayout(null);
		track4.setBounds(650,410,200,30);
		track4.setVisible(false);
		add(track4);
		
		T4Pbness = new JProgressBar();
		T4Pbness.setLayout(null);
		T4Pbness.setBounds(650,480,400,30);
		T4Pbness.setValue(20);
		T4Pbness.setForeground(new Color(171,0,19));
		T4Pbness.setVisible(false);
		add(T4Pbness);
		
		T4Pbopt = new JProgressBar();
		T4Pbopt.setLayout(null);
		T4Pbopt.setBounds(650,530,400,30);
		T4Pbopt.setValue(20);
		T4Pbopt.setForeground(new Color(171,0,19));
		T4Pbopt.setVisible(false);
		add(T4Pbopt);
		
		track5 = new JButton("track5");
		track5.setFont(new Font("",Font.BOLD, 25));
		track5.setLayout(null);
		track5.setBounds(1250,410,200,30);
		track5.setVisible(false);
		add(track5);
		
		T5Pbness = new JProgressBar();
		T5Pbness.setLayout(null);
		T5Pbness.setBounds(1250,480,400,30);
		T5Pbness.setValue(20);
		T5Pbness.setForeground(new Color(171,0,19));
		T5Pbness.setVisible(false);
		add(T5Pbness);
		
		T5Pbopt = new JProgressBar();
		T5Pbopt.setLayout(null);
		T5Pbopt.setBounds(1250,530,400,30);
		T5Pbopt.setValue(20);
		T5Pbopt.setForeground(new Color(171,0,19));
		T5Pbopt.setVisible(false);
		add(T5Pbopt);
		
		
	}
	
	private class BtnListener implements MouseListener{
        public void mouseClicked(MouseEvent event) {}
        public void mousePressed(MouseEvent event) {}
        public void mouseReleased(MouseEvent event) {
        	JButton btn = (JButton)event.getSource();
        	
        	if(btn == searchBtn) {
        		studNameShowLbl.setVisible(true);
        		studIdShowLbl.setVisible(true);
        		studMajorShowLbl.setVisible(true);
        		studSemesterShowLbl.setVisible(true);
        		
        		track1.setVisible(true);
        		track2.setVisible(true);
        		track3.setVisible(true);
        		track4.setVisible(true);
        		track5.setVisible(true);
        		T1Pbness.setVisible(true);
        		T1Pbopt.setVisible(true);
        		T2Pbness.setVisible(true);
        		T2Pbopt.setVisible(true);
        		T3Pbness.setVisible(true);
        		T3Pbopt.setVisible(true);
        		T4Pbness.setVisible(true);
        		T4Pbopt.setVisible(true);
        		T5Pbness.setVisible(true);
        		T5Pbopt.setVisible(true);
        		repaint();
        		
        		//db에서 학번으로 검색해서 값 얻어와야됌
        	}
        	
        	if(btn == track1) {
        		
        		
        	}
        	
        	if(btn == track2) {
        		
        		
        	}
        	
        	if(btn == track3) {
        		
        		
        	}
        	
        	if(btn == track4) {
        		
        		
        	}
        	
        	if(btn == track5) {
        		
        		
        	}
        	
        }
        public void mouseEntered(MouseEvent event) {}
        public void mouseExited(MouseEvent event) {}         
     }//BtnMenuListener
}
