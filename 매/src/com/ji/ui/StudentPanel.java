package com.ji.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

public class StudentPanel extends JPanel {
	
	private JLabel name;
	private JPanel uppperPanel;
	private JPanel lowerPanel;
	
	private JButton logOutBtn;
	private JButton recommendSubBtn;
	private JButton recommendTrackBtn;
	private JButton updateInfoBtn;
	private BtnListener btnL;
	
	private RecommendTrackPanel recommendTrackPanel;
	private RecommendSubPanel recommendSubPanel;
	private UpdateInfoPanel updateInfoPanel;

	
	public StudentPanel(String userName) {
		setPreferredSize(new Dimension(1900,950));
		btnL = new BtnListener();
		
		uppperPanel = new JPanel();
		uppperPanel.setLayout(null);
		uppperPanel.setBounds(0,0,1900,150);
		uppperPanel.setBackground(new Color(171,0,19));
		add(uppperPanel);
		
		String text = userName;
		name = new JLabel("안녕하세요, "+text+" 님!");
		name.setFont(new Font("",Font.BOLD, 15));
		name.setForeground(Color.white);
		name.setBounds(1650,50,200,20);
		uppperPanel.add(name);
	
		logOutBtn = new JButton("LOGOUT");
		logOutBtn.setFont(new Font("",Font.BOLD, 10));
		logOutBtn.setBounds(1700,80,100,30);
		logOutBtn.addMouseListener(btnL);
		logOutBtn.setBackground(Color.WHITE);
		uppperPanel.add(logOutBtn);	

		recommendSubBtn = new JButton("Recommand Subject");
		recommendSubBtn.setFont(new Font("",Font.BOLD, 15));
		recommendSubBtn.setBounds(150,70,200,30);
		recommendSubBtn.setBackground(Color.WHITE);
		recommendSubBtn.addMouseListener(btnL);
		uppperPanel.add(recommendSubBtn);	

		recommendTrackBtn = new JButton("Recommend Track");
		recommendTrackBtn.setFont(new Font("",Font.BOLD, 15));
		recommendTrackBtn.setBounds(450,70,200,30);
		recommendTrackBtn.setBackground(Color.WHITE);
		recommendTrackBtn.addMouseListener(btnL);
		uppperPanel.add(recommendTrackBtn);	

		updateInfoBtn = new JButton("Update Infomation");
		updateInfoBtn.setFont(new Font("",Font.BOLD, 15));
		updateInfoBtn.setBounds(750,70,200,30);
		updateInfoBtn.setBackground(Color.WHITE);
		updateInfoBtn.addMouseListener(btnL);
		uppperPanel.add(updateInfoBtn);	


		lowerPanel = new JPanel();
		lowerPanel.setLayout(null);
		lowerPanel.setBounds(0,150,1900,800);
		add(lowerPanel);
		
		recommendSubPanel = new RecommendSubPanel();
		recommendSubPanel.setLayout(null);
		recommendSubPanel.setBounds(0,0,1900,800);
		
		lowerPanel.add(recommendSubPanel);
		
	}

	private class BtnListener implements MouseListener{
        public void mouseClicked(MouseEvent event) {}
        public void mousePressed(MouseEvent event) {}
        public void mouseReleased(MouseEvent event) {
        	JButton btn = (JButton)event.getSource();
        	
        	if(btn == logOutBtn) {
        		removeAll();
        		
        		LoginPanel loginPanel = new LoginPanel();
        		loginPanel.setLayout(null);
        		loginPanel.setBounds(0,0,1900,950);
				add(loginPanel);
				loginPanel.setVisible(true);
				
        		repaint();
        	}
        	if(btn == recommendSubBtn) {
        		lowerPanel.removeAll();
        		
        		recommendSubPanel = new RecommendSubPanel();
        		recommendSubPanel.setLayout(null);
        		recommendSubPanel.setBounds(0,0,1900,800);
        		
        		lowerPanel.add(recommendSubPanel);
        		repaint();
        	}//전체보기
        	
        	if(btn == recommendTrackBtn) {
        		lowerPanel.removeAll();

        		recommendTrackPanel = new RecommendTrackPanel();
        		recommendTrackPanel.setLayout(null);
        		recommendTrackPanel.setBounds(0,0,1900,800);
        		
        		lowerPanel.add(recommendTrackPanel);
        		repaint();
        	}//학생 검색
        	
        	if(btn == updateInfoBtn) {
        		lowerPanel.removeAll();
        		
        		updateInfoPanel = new UpdateInfoPanel();
        		updateInfoPanel.setLayout(null);
        		updateInfoPanel.setBounds(0,0,1900,800);
        		
        		lowerPanel.add(updateInfoPanel);
        		
        		repaint();
        	}//트랙별 과목 정보 수정
        }
        public void mouseEntered(MouseEvent event) {}
        public void mouseExited(MouseEvent event) {}         
     }//BtnMenuListener
	
}
