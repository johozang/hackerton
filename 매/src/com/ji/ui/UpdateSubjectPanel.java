package com.ji.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class UpdateSubjectPanel extends JPanel {
	
	private JLabel trackNameLbl,subjectNameLbl,typeLbl,funcLbl,newSubjectLbl;
	private JComboBox<String> trackNameCb,subjectNameCb,typeCb,funcCb;
	private JTextField newSubjectTf;
	private JButton update;
	
	private String trackNameArr[] = {"사물인터넷","멀티미디어","시스템응용","인공지능","가상현실"};
	private String subjectNameArr[] = {"dsdf","sdf"};//트랙별로 DB에서 받아와야함
	private String typeArr[] = {"필수","선택"};
	private String funcArr[] = {"추가","삭제","수정"};
	
	private BtnListener btnL;
	
	Vector<String> col = new Vector<String>();
	DefaultTableModel model;
	Vector<String> row;
	JTable table;
	
	public UpdateSubjectPanel() {
		setPreferredSize(new Dimension(1900,800));
		
		btnL = new BtnListener();
		
		trackNameLbl = new JLabel("트랙 이름");
		trackNameLbl.setLayout(null);
		trackNameLbl.setFont(new Font("",Font.BOLD, 15));
		trackNameLbl.setBounds(20,10,70,50);
		add(trackNameLbl);
		
		subjectNameLbl= new JLabel("과목명");
		subjectNameLbl.setFont(new Font("",Font.BOLD, 15));
		subjectNameLbl.setLayout(null);
		subjectNameLbl.setBounds(400,10,50,50);
		add(subjectNameLbl);
		
		typeLbl= new JLabel("종류");
		typeLbl.setLayout(null);
		typeLbl.setFont(new Font("",Font.BOLD, 15));
		typeLbl.setBounds(780,10,50,50);
		add(typeLbl);
		
		funcLbl= new JLabel("기능");
		funcLbl.setLayout(null);
		funcLbl.setFont(new Font("",Font.BOLD, 15));
		funcLbl.setBounds(1150,10,50,50);
		add(funcLbl);
		
		newSubjectLbl= new JLabel("수정 할 이름");
		newSubjectLbl.setLayout(null);
		newSubjectLbl.setFont(new Font("",Font.BOLD, 15));
		newSubjectLbl.setBounds(1450,10,100,50);
		newSubjectLbl.setVisible(false);
		add(newSubjectLbl);
		
		trackNameCb = new JComboBox<String>(trackNameArr);
		trackNameCb.setLayout(null);
		trackNameCb.setBounds(100,10,200,50);
		trackNameCb.setBackground(Color.WHITE);
		add(trackNameCb);
		
		subjectNameCb = new JComboBox<String>(subjectNameArr);
		subjectNameCb.setLayout(null);
		subjectNameCb.setBounds(480,10,200,50);
		subjectNameCb.setBackground(Color.WHITE);
		add(subjectNameCb);
		
		typeCb = new JComboBox<String>(typeArr);
		typeCb.setLayout(null);
		typeCb.setBounds(850,10,200,50);
		typeCb.setBackground(Color.WHITE);
		add(typeCb);

		funcCb = new JComboBox<String>(funcArr);
		funcCb.setLayout(null);
		funcCb.setBounds(1230,10,150,50);
		funcCb.setBackground(Color.WHITE);
		add(funcCb);
				
		newSubjectTf = new JTextField();
		newSubjectTf.setLayout(null);
		newSubjectTf.setBounds(1570,10,200,50);
		newSubjectTf.setBackground(Color.WHITE);
		newSubjectTf.setVisible(false);
		add(newSubjectTf);
		
		
		update = new JButton("GO");
		update.setLayout(null);
		update.setBounds(1800,10,70,50);
		update.setForeground(Color.black);
		update.setBackground(Color.WHITE);
		update.addMouseListener(btnL);
		add(update);
		
		
	}
	
	private class BtnListener implements MouseListener{
        public void mouseClicked(MouseEvent event) {}
        public void mousePressed(MouseEvent event) {}
        public void mouseReleased(MouseEvent event) {
        	JButton btn = (JButton)event.getSource();
        	
        	if(btn == update) {
        		newSubjectLbl.setVisible(true);
        		newSubjectTf.setVisible(true);
        		
        	}
        }
        public void mouseEntered(MouseEvent event) {}
        public void mouseExited(MouseEvent event) {}         
     }//BtnMenuListener
}
