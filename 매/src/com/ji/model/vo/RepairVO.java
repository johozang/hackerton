package com.ji.model.vo;

public class RepairVO {
	 private int repairId;
	 private String name;
	 private String address;
	 private String number;
	 private String masterName;
	 private String email;
	 
	 public RepairVO() {}
	 
	 public RepairVO(int repairId, String name, String address, String number,
			 String email, String masterName) {
		 this.repairId = repairId;
		 this.name = name;
		 this.address = address;
		 this.number = number;
		 this.email = email;
		 this.masterName = masterName;
	 }
	 
	 public int getrepairId() {return this.repairId;}
	 public void setrepairId(int repairId) {this.repairId = repairId;}

	 public String getname() {return this.name;}
	 public void setname(String name) {this.name = name;}
	 
	 public String getaddress() {return this.address;}
	 public void setaddress(String address) {this.address = address;}
	 
	 public String getnumber() {return this.number;}
	 public void setnumber(String number) {this.number = number;}
	 
	 public String getemail() {return this.email;}
	 public void setemail(String email) {this.email = email;}
	 
	 public String getmasterName() {return this.masterName;}
	 public void setmasterName(String name) {this.masterName = name;}
	 
	 public String toString() {
		 return "CustomerVO [repairId="+repairId+",name="+name+",address="+address+",number="+number
				 +",email="+email+",masterName="+masterName;
	 }
}
