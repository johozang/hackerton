package com.ji.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.CarVO;

public class CarDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private CarDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static CarDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static CarDAO getInstance() {
		if(obj==null) {
			obj=new CarDAO();
		}
		return obj;
	}
	
	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<CarVO> subQueryCar(String select1, String select2, String from1, String from2, 
			String where1, String where2, String in){
		String sql = "select "+select1+" from "+from1+" where "+where1;
		if(   !(in == null || in.equals(""))   ) {
			sql += " in "+in; //where절
		}
		if(   !(select2 == null || select2.equals(""))   ) {
			sql += " (select "+select2; //where절
		}
		if(   !(from2 == null || from2.equals(""))   ) {
			sql += " from "+from2; //where절
		}
		if(   !(where2 == null || where2.equals(""))   ) {
			sql += " where "+where2+");"; //where절
		}
		
		PreparedStatement pstmt = null;
		
		ArrayList<CarVO> arrCarVO = new ArrayList<CarVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				CarVO tempCarVO = new CarVO(rs.getInt("carId"),
						rs.getInt("compId"),
						rs.getString("name"),
						rs.getInt("carNum"),
						rs.getInt("carMenber"),
						rs.getInt("rentFee"),
						rs.getInt("regiDate"),
						rs.getString("details"));
				arrCarVO.add(tempCarVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrCarVO;
	}//sub_query
	
	
	public ArrayList<CarVO> selectCar(String condition){
		String sql = "select * from Car";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		ArrayList<CarVO> arrCarVO = new ArrayList<CarVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				CarVO tempCarVO = new CarVO(rs.getInt("carId"),
						rs.getInt("compId"),
						rs.getString("name"),
						rs.getInt("carNum"),
						rs.getInt("carMenber"),
						rs.getInt("rentFee"),
						rs.getInt("regiDate"),
						rs.getString("details"));
				arrCarVO.add(tempCarVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrCarVO;
	}//select
	
	public void insertCar(int carId, int compId, String name, int carNum, int carMember,
			 int rentFee, int regiDate, String details){
		
		String sql = "insert into Car values (?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,carId);
			pstmt.setInt(2,compId);
			pstmt.setString(3,name);
			pstmt.setInt(4,carNum);
			pstmt.setInt(5,carMember);
			pstmt.setInt(6,rentFee);
			pstmt.setInt(7,regiDate);
			pstmt.setString(8,details);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}//insert
	
	public void updateCar(int carId, int compId, String name, int carNum, int carMember,
			 int rentFee, int regiDate, String details,String condition, String carImage){
		
		String sql = "update Car set  (?,?,?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,carId);
			pstmt.setString(2,name);
			pstmt.setInt(3,carNum);
			pstmt.setInt(4,carMember);
			pstmt.setString(5,carImage);
			pstmt.setInt(6,rentFee);
			pstmt.setInt(7,compId);
			pstmt.setInt(8,regiDate);
			pstmt.setString(9,details);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}//update
	
	public void deleteCar(int carId,String condition){
		
		String sql = "delete from Car";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,carId);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}//delete
	
}
