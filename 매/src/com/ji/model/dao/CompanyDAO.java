package com.ji.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.CompanyVO;

public class CompanyDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private CompanyDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static CompanyDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static CompanyDAO getInstance() {
		if(obj==null) {
			obj=new CompanyDAO();
		}
		return obj;
	}
	
	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<CompanyVO> selectCompany(String condition){
		String sql = "select * from Company";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		ArrayList<CompanyVO> arrCompanyVO = new ArrayList<CompanyVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				CompanyVO tempCompanyVO = new CompanyVO(rs.getInt("compId"),
						rs.getString("name"),
						rs.getString("address"),
						rs.getString("number"),
						rs.getString("masterName"),
						rs.getString("email"));
				arrCompanyVO.add(tempCompanyVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrCompanyVO;
	}//insert
	
	public void insertCompany(int compId, String name, String address, String number,
			 String email, String masterName){
		
		String sql = "insert into Company values (?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,compId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,number);
			pstmt.setString(5,masterName);
			pstmt.setString(6,email);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateCompany(int compId, String name, String address, String num,
			 String email, String masterName,String condition){
		
		String sql = "update Company set  (?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,compId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,num);
			pstmt.setString(5,masterName);
			pstmt.setString(6,email);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void deleteCompany(int compId,String condition){
		
		String sql = "delete from Company";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,compId);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
